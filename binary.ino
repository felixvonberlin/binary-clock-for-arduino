/**
    Binary Clock for Arduino
    Copyright (C) 2018  Felix O.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using namespace std;
#define ELEMENTCOUNT(x)  (sizeof(x) / sizeof(x[0])) 
int ledH [4]{6,5,4,3};
int ledAMPM = 2;
int ledM [6] {12,11,10,9,8,7};
long blinks [4] {400,1000,3000,5000};
int hour = 0;
int minute = 0;
const int button = 13;
bool Pm = false;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(button,INPUT);
  digitalWrite(button,HIGH);
  
    for (int x = 0; x < ELEMENTCOUNT(ledM);x++){
      pin(ledM[x],true);
      if (x < ELEMENTCOUNT(ledH))
        pin(ledH[x],true);
    }
    delay(2000);
    for (int x = 0; x < ELEMENTCOUNT(ledM);x++){
      pin(ledM[x],false);
      if (x < ELEMENTCOUNT(ledH))
        pin(ledH[x],false);
    }
  while(true){
    unsigned long pressed = 0;
    while(digitalRead(button)){}
    while (!digitalRead(button)){
      pressed = pressed + 200;
      for (int u = 0; u < ELEMENTCOUNT(blinks);u++)
        if (pressed == blinks[u])
          pin(ledAMPM,true);
      delay(200);
      for (int u = 0; u < ELEMENTCOUNT(blinks);u++)
        if (pressed == blinks[u])
          pin(ledAMPM,false);
     }
     if (pressed <= 400){
      if (hour < 12)
        hour++;
      else
        hour = 0;
      printH(hour);
     }else if (pressed <= 1000){
      if (minute < 59)
        minute++;
      else
       minute = 0;
      printM(minute);
     }else if (pressed <= 3000){
        Pm = !Pm;
     }else{
       for (int l = 0; l < 2;l++){
        bool bo = l % 2 == 0;
        for (int o = 0; o < ELEMENTCOUNT(ledM);o++){
            pin(ledM[o],true);
            delay(100);
            pin(ledM[o],false);
          }
        for (int o = 0; o < ELEMENTCOUNT(ledH);o++){
            pin(ledH[o],true);
            delay(100);
            pin(ledH[o],false);
          }
        }
        break;
     }
  }
  delay(1000);
}

void pin(int pin,bool state){
  if (state)
    analogWrite(pin,255);
  else
    analogWrite(pin,0);
}

void getBinary(bool* pins,int number){  
  String bin = "";
  int x = number;
  while (true){
    int y = x % 2;
    bin =  bin + y;
    if (x / 2 == 0)
      break;
    x = x / 2;
    }
    pins  [(bin.length())];
  for (int s = 0; s < bin.length();s++){
    if (bin[s] == '0')
      pins[s] = false;
    else
      pins[s] = true;
    }
}
void printH(int h){
    bool pinsH [4] {};
    getBinary(pinsH,h);
    for (int p = 0; p < ELEMENTCOUNT(pinsH);p++){
      pin(ledH[p],pinsH[p]);
    }
  }
void printM(int m){
    bool pinsM [6] {};
    getBinary(pinsM,m);
    for (int n = 0; n < ELEMENTCOUNT(pinsM);n++){
      pin(ledM[n],pinsM[n]);
    }
  }
void loop() {
  bool pinsH [4] {};
  bool pinsS [6] {};

  if (minute >= 60){
    hour++;
    minute = 0;
  }
  if (Pm)
    if (hour >= 12){
      hour = 0;
      Pm = false;
    }
  else
    if (hour >= 12){
      hour = 1;
      Pm = true;
    }
  printH(hour);
  printM(minute);
  pin(ledAMPM,Pm);
  minute++;
  delay(60062.8691111);
}
