[![Arduino](https://img.shields.io/badge/designed%20for-Arduino-green.svg?logo=arduino&color=00979d)](https://www.arduino.cc)

# Binary Clock for Arduino Uno
It's a simple binary clock for Arduino.

There is not much documentation, because it was only a project for learing how to use this board.

But if you want to use or improve it, feel free to contact me under [felix@von-oertzen-berlin.de](mailto:felix@von-oertzen-berlin.de).